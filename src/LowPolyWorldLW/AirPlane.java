package LowPolyWorldLW;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 * test
 * @author alex
 */

public class AirPlane implements AnimEventListener {
    
    private Node rootNode;
    private AssetManager assetManager;
    
    private Node model;
    private Spatial plane;
    
    private AnimControl control;
    private AnimChannel propeller;
    private AnimChannel aerobatic;    
    
    private static final Vector3f START_POINT = new Vector3f(2f, 3f, -10f);

    
    public AirPlane(Node rootNode, AssetManager assetManager) {
        
        this.rootNode = rootNode;
        this.assetManager = assetManager;
        
    }
    
    private void loadModel() {
        
        // Load plane
        model = (Node)assetManager.loadModel("Models/plane.j3o");
        model.setLocalTranslation(START_POINT);
        model.setShadowMode(RenderQueue.ShadowMode.Cast);
        rootNode.attachChild(model);
        
        plane = model.getChild("Plane");
        
    }
    
    // TODO
    public void playAnimation(int number) {
        
        loadModel();
        
        // Animation
        control = plane.getControl(AnimControl.class);
        control.addListener(this);
        propeller = control.createChannel();
        aerobatic = control.createChannel();
        propeller.setAnim("Propeller");
        aerobatic.setAnim("Plane");                     // Random aerobatic maneuver
        aerobatic.setLoopMode(LoopMode.DontLoop);
        propeller.setLoopMode(LoopMode.DontLoop);
        
    }
    
    // TODO
    public void playRandomAnimation() {
        
    }

    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {

        rootNode.detachChild(model);
        
    }

    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {

    }
    
}
