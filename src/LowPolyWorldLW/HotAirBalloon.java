package LowPolyWorldLW;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;

/**
 * test
 * @author alex
 */

public class HotAirBalloon extends AbstractAppState {
    
    private SimpleApplication app;
    private Node rootNode;
    private AssetManager assetManager;
    private Node balloon;
    
    
    
    private class BalloonControl extends AbstractControl {

        @Override
        protected void controlUpdate(float tpf) {
            
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {   }
        
    }
    
    private void loadBalloon() {
        
        this.balloon = (Node)assetManager.loadModel("Blender/terra.j3o");        
        balloon.addControl(new BalloonControl());      
        balloon.setShadowMode(RenderQueue.ShadowMode.Cast);
        rootNode.attachChild(balloon);
        
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
    
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();
        
                
        loadBalloon();
    }
    
}
