package LowPolyWorldLW;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;

/**
 * test
 * @author alex
 */

public class Terra extends AbstractAppState {
    
    private SimpleApplication app;
    private Node rootNode;
    private AssetManager assetManager;
    private Node world;
    
    private static final float ROTATION_SPEED = 0.1f;
    
    
    private class TerraControl extends AbstractControl {

        @Override
        protected void controlUpdate(float tpf) {
            
            spatial.rotate(new Quaternion().fromAngleAxis(ROTATION_SPEED * tpf, Vector3f.UNIT_Z));
            
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {   }
        
    }
    
    private void loadWorld() {
        
        this.world = (Node)assetManager.loadModel("Models/terra.j3o");        
        world.addControl(new TerraControl());      
        world.setName("Terra");
        world.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        rootNode.attachChild(world);
        
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
    
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();
        
                
        loadWorld();
    }
    
}
