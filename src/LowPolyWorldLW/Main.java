package LowPolyWorldLW;

import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.renderer.RenderManager;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.shadow.EdgeFilteringMode;
import com.jme3.shadow.PointLightShadowFilter;
import com.jme3.shadow.PointLightShadowRenderer;
import com.jme3.system.AppSettings;

/**
 * test
 * @author alex
 */
public class Main extends SimpleApplication {

    private static final int SHADOW_MAP_SIZE = 1024;
    private static final Vector3f LIGHT1_DIRECTION = new Vector3f(.3f, -.5f, -.5f);
    
    private static final Vector3f LIGHT2_LAMPPOSITION = new Vector3f(0f, 8f, 2f);
    private static final float LIGHT2_LAMPRADIUS = 20f;
    
    private static final Vector3f CAM_POSITION = new Vector3f(-0.89554244f, 4.9244604f, 3.4644043f);
    private static final Vector3f CAM_DIRECTION = new Vector3f(0.05378858f, 0.02218004f, -0.9983058f);
        
    
    @Override
    public void simpleInitApp() {
        
        setupCam();        
//        setupLight1();
        setupLight2();
       
        // attach terra
        Terra terra = new Terra();
        stateManager.attach(terra);
        
        // attach cinematic manager
        AnimationManager manager = new AnimationManager();
        stateManager.attach(manager);
        


    }

    private void setupCam() {
        
        flyCam.setMoveSpeed(20f);
        viewPort.setBackgroundColor(ColorRGBA.Blue);
        
        cam.setLocation(CAM_POSITION);
        cam.lookAtDirection(CAM_DIRECTION, CAM_POSITION);
        
    }
    // Shadow Light variant 1
    private void setupLight1() {    
        
        // Ambient
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.Blue);
        rootNode.addLight(ambient);        
         
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(LIGHT1_DIRECTION);        
        rootNode.addLight(sun);
        
        DirectionalLightShadowRenderer dlsr = new DirectionalLightShadowRenderer(assetManager, SHADOW_MAP_SIZE, 2);
        dlsr.setLight(sun);        
        viewPort.addProcessor(dlsr);
        
    }
    
    // Shadow Light variant 2
    private void setupLight2() {
                        
        // Point Light
        PointLight lamp = new PointLight();
        lamp.setColor(ColorRGBA.White.mult(2f));
        lamp.setRadius(LIGHT2_LAMPRADIUS);
        lamp.setPosition(LIGHT2_LAMPPOSITION);
        rootNode.addLight(lamp);        
        
        PointLightShadowRenderer renderer = new PointLightShadowRenderer(assetManager, SHADOW_MAP_SIZE);
        renderer.setLight(lamp);
        renderer.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        viewPort.addProcessor(renderer);
        
        PointLightShadowFilter filter = new PointLightShadowFilter(assetManager, SHADOW_MAP_SIZE);
        filter.setLight(lamp);
        filter.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        filter.setEnabled(false);
        
        // Fpp
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        fpp.addFilter(filter);
        viewPort.addProcessor(fpp);

    }

    @Override
    public void simpleUpdate(float tpf) {
        // TODO
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
     public static void main(String[] args) {
         
        Main app = new Main();
        
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 576);
//        settings.setSamples(4);
        
        app.setSettings(settings);
        app.setShowSettings(false);

        app.start();

     }
    
}
