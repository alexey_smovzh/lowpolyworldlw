package LowPolyWorldLW;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;

/**
 * test
 * @author alex
 */

public class AnimationManager extends AbstractAppState {
    
    private SimpleApplication app;
    private Node rootNode;
    private AssetManager assetManager;
    
    
    private AirPlane airPlane;
    
    private static final int OCEAN1 = 0;
    private static final int CURORT = 1;
    private static final int OCEAN2 = 2;
    private static final int INDUSTRIAL = 3;
    private static final int FOREST = 4;
    private static final int WINTER = 5;
    
    
    public AnimationManager() {
    }
    
    // Choose animation based on location
    private void startAnimation(int location) {
        
        airPlane.playAnimation(0);
        
    }
    
    boolean singularity = false;
    int flag = 1000;
    @Override
    public void update(float tpf) {
        
        // 0 -> 1.57 = 90
        // 1.57 -> 0 = 180
        // 0 -> -1.57 = 270
        // -1.57 -> 0 = 360
        //
        // http://hub.jmonkeyengine.org/t/problem-with-getlocalrotation-fromangleaxis/24664
        
        float angle = rootNode.getChild("Terra").getLocalRotation().toAngles(null)[2];
        int z = (int)(angle * 100);
       
        if(angle > 1.57) {                                                      // Noth-South pole singularity
            singularity = true;
        } else if (angle < -1.57) {
            singularity = false;
        }
            
        if(flag != z) {
            if(z == 0 && singularity == false) {
                System.out.println("Ocean 1");                        
            } else if(z == 75 && singularity == false) {
                startAnimation(CURORT);                
                System.out.println("Curort"); 
            } else if(z == 114 && singularity == true) {
                System.out.println("Ocean 2");    
            } else if(z == 23 && singularity == true) {
                System.out.println("Industrial");                
            } else if(z == -80 && singularity == true) {
                System.out.println("Forest");    
            } else if(z == -135 && singularity == false) {
                System.out.println("Winter");    
            }
            
            flag = z;
        }
    }        
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
    
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();            

        
        airPlane = new AirPlane(rootNode, assetManager);
        
    }
    
}
